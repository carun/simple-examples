= Distributed Component part source2

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative: {page-relative} 

== Another page from source2

Here's a link to the content in the other part:

----
xref::index.adoc[from the other component source]
----

xref::index.adoc[from the other component source]

