= {page-component-title}

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative-src-path: {page-relative-src-path}


== Shared content with xrefs

Component 2 includes the three well-organized pages from the `shared` component in a similar structure as in the source component, twice.
As these pages are not intended to appear standalone, they are `partials` in the `shared` component.
The shared component has these pages in the `ROOT` module, and they are included once into `module1` in this component, successfully.

=== A limitation

The three shared pages are also included again into a topic `submodule` in `module2`.
In this case, the pages appear but the links do not resolve properly.
For the xref links in the included partials to work, the position in the module must match the expectations expressed in the xrefs.

== Links

Each page has three links to each of the other two pages:

* To the page itself.
* To the section for the links to the first of the linked pages.
* To the section for the links to the second of the linked pages.

The links between pages use the coordinates of the page expected to include them, although the links are in the included pages.
As a partial is not itself a page, it is not possible for an xref to point to a partial itself.
Instead, it can point to a page that includes the partial.
