= {page-component-title}
:page-aliases: master@{page-component-name}:{page-module}:{page-relative}

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative: {page-relative} 

page-aliases: {page-aliases}

== A home page

link:../index.html[direct link link:../index.html to static bounce page]

An xref to an alias currently is not computed:

xref:master@component1:{page-module}:{page-relative}[xref link to alias master@component1:{page-module}:{page-relative}]
