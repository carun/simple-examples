= {page-component-title}

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative-src-path: {page-relative-src-path}


== This page demonstrates all sorts of includes from examples

=== relative only, this component, version, module

include::example$example1.txt[]

include::example$src/main/java/org/example2.txt[]

=== module and relative: this component, version

include::module1:example$example3.txt[]

include::module1:example$src/main/java/example4.txt[]

=== version and relative, this component and module

include::2.0@:example$example5.txt[]

include::2.0@:example$src/main/java/org/example6.txt[]

=== version, module, and relative, this component

include::2.0@module1:example$example7.txt[]

include::2.0@module1:example$src/main/java/example8.txt[]

=== version, compoment, and relative, (ROOT) module

include::1.0@component2::example$example9.txt[]

include::1.0@component2::example$src/main/java/org/example10.txt[]

=== version, component, module, and relative

include::1.0@component2:module1:example$example11.txt[]

include::1.0@component2:module1:example$src/main/java/example12.txt[]

=== component and relative; default (master) version and default (ROOT) module.

include::component2::example$example13.txt[]

include::component2::example$src/main/java/org/example14.txt[]

=== component, module, and relative; default (master) version.

include::component2:module1:example$example15.txt[]

include::component2:module1:example$src/main/java/example16.txt[]


== Here is an example of including content from a page in another module

include::module1:page$sub-page.adoc[]
