= {description}
:description: indexTemplate Examples

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative-src-path: {page-relative-src-path}


== Examples of `indexTemplate` block processor


[indexTemplate, 'resourceid',family=partial,module=module1]
----
\include::{resourceid}[]
----
