= {description}
:description: indexList Examples
//Uncomment to see lists logged to the console.
//:antora-indexer-log-lists:

== Page Coordinates


page-component-name: component

page-component-version: 1.0

page-module: ROOT

page-relative-src-path: {page-relative-src-path}

NOTE: This page can use the header attribute `antora-indexer-log-lists` to log the text .adoc representation of the generated lists to the console.

== Examples of `indexList` block macro

indexList::[level=1,style=square]

Next we expect a list:

[square]
* this is a first list item

indexList::[level=1]

* this is a second list item
** this is a sub item
*** third level

indexList::[level=1,style=square]

* this is a third list item
** this is a sub item
*** third level

indexList::[level=2]

* this is a fourth list item
** this is a sub item
*** third level

indexList::[level=3]

* this is a fifth list item
** this is a sub item
*** third level

indexList::[level=4]

this is not a list item

* next should be separate

indexList::[module=module1,style=square]

next has relative filter

indexList::[component=*,version=*,module=*,relative=topic*/*.adoc,style=square]

Finally we show a format expression

indexList::[`Link: ${$xref} ($\{description})`,component=*,version=*,module=*,relative=topic*/*.adoc,style=square]
