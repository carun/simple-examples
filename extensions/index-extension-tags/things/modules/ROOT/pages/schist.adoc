= Schist
:tag: mineral
:description: Schist is a medium grade metamorphic rock with sufficient foliation that it may be split along the layers.

{description}

== Related Pages

indexDescriptionList::[attributes='tag=mineral',descAttribute=description,style=horizontal]
